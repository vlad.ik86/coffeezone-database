-- Author Vladyslav Berezhnyak
-- Created by Mockaroo (https://mockaroo.com)

-- Data: PRODUCTS
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0001', 'Latte', 9.85, 'R0001');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0002', 'Cappuccino', 7.75, 'R0002');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0003', 'Espresso', 4.75, 'R0003');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0004', 'Decaf', 5.50, 'R0004');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0005', 'Moca', 6.95, 'R0005');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0006', 'Macchiato', 4.95, 'R0006');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0007', 'Americano', 9.95, 'R0007');
insert into PRODUCTS (prod_id, name, retail, recipe_id) values ('P0008', 'Doppio', 4.95, 'R0008');

-- Data: RECIPE
insert into RECIPE (recipe_id, prod_id) values ('R0001', 'P0001');
insert into RECIPE (recipe_id, prod_id) values ('R0002', 'P0002');
insert into RECIPE (recipe_id, prod_id) values ('R0003', 'P0003');
insert into RECIPE (recipe_id, prod_id) values ('R0004', 'P0004');
insert into RECIPE (recipe_id, prod_id) values ('R0005', 'P0005');
insert into RECIPE (recipe_id, prod_id) values ('R0006', 'P0006');
insert into RECIPE (recipe_id, prod_id) values ('R0007', 'P0007');
insert into RECIPE (recipe_id, prod_id) values ('R0008', 'P0008');

-- Data: INGREDIENTS
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN001', 'Milk', 200, 10000);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN002', 'Beans', 500, 100);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN003', 'Sugar', 350, 100);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN004', 'Brown Sugar', 125, 150);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN005', 'Cream', 100, 100);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN006', 'Cinammon', 225, 25);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN007', 'Water', 50, 10000);
insert into INGREDIENTS (ingr_id, name, restock_price, instock_qty) values ('IN008', 'Soybean', 75, 50);

-- Data: RECIPE_INGREDIENTS
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN007', 250, 'R0001');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN002', 5, 'R0001');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN003', 3, 'R0001');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN006', 1, 'R0001');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN007', 250, 'R0002');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN008', 4, 'R0002');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN004', 2, 'R0002');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN006', 2, 'R0002');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN001', 350, 'R0003');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN002', 2, 'R0003');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN003', 3, 'R0003');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN005', 4, 'R0003');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN007', 350, 'R0004');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN002', 3, 'R0004');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN004', 1, 'R0004');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN005', 4, 'R0004');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN001', 450, 'R0005');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN002', 2, 'R0005');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN003', 4, 'R0005');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN006', 2, 'R0005');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN001', 375, 'R0006');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN008', 2, 'R0006');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN003', 1, 'R0006');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN005', 4, 'R0006');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN007', 225, 'R0007');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN002', 3, 'R0007');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN004', 4, 'R0007');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN005', 3, 'R0007');

insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN001', 175, 'R0008');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN008', 1, 'R0008');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN004', 2, 'R0008');
insert into RECIPE_INGREDIENTS (ingr_id, qty, recipe_id) values ('IN006', 3, 'R0008');

-- Data: APP_IMAGES                ADD MANUALLY IN THE SAME ORDER!!!!!!
/*
insert into APP_IMAGES (image_name, image_blob) values (logo_orange, NULL);
insert into APP_IMAGES (image_name, image_blob) values (main_coffee, NULL);
insert into APP_IMAGES (image_name, image_blob) values (coffee, NULL);
insert into APP_IMAGES (image_name, image_blob) values (user, NULL); -- 'user' IS YOUR OWN USER ID!!!!!!!!
insert into APP_IMAGES (image_name, image_blob) values (logo_black, NULL);
*/










