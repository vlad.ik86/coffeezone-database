# Anteiku - A CoffeeZone DB2 Project
>Nicolae Rusu, Vladyslav Berezhnyak, Luke Weaver

On the main screen, users have the option to Browse a guest shop, login, and register.

The header, present on every screen, has a browse button which leads the user to the Guest Shop if they are not logged in, or to the Regular Shop if logged in.

The login screen takes a username and password, hashes it by taking the database-stored salt, and compares it with the database-stored hash. 

> ***See Problems***
>If the comparison is valid, it calls a procedure which logs the login time in a database table, changes the global variable username, and shows the Regular Shop.
    
The register screen takes the needed information for a new customer, generates a random salt, hashes the password, and stores all of those fields in the database.
  >The register procedure also adds +5 to the column containing the referring customer's discount amount if his referral has been used less than 3 times. It then goes to the login screen.

The Guest Shop shows products alongside their images and price, but they cannot add to cart. The user has an option to login to checkout.

The Regular Shop shows the same information, but with buttons to add or subtract the quantity in cart, the quantity itself, and the total price of the cart.
 >On the side, a field retrieves the user's database-stored address and gives an option to change it when clicking on it.
    It is a text field with a transparent background that updates the database when checking out.
    When the page initializes, it calls a function to get the discount amount available for the user.
    
Pressing the checkout button calls a function that checks if there are enough ingredients to make the coffees in the order and
 > **a.** returns "no" if there is a quantity problem, which alerts the user of it and lets them resubmit a new order.
 
 > **b.** returns "yes" if everything is fine, adds the order details to a table, and alerts the user of the successful order.

It also updates the stored address with the newly typed address.


# Running
Our application is run from the **Main.java** file.
**JLServices.java** is setup to a database where everything is already setup. To setup your own database, run **schema.sql**, **data.sql**, and **procs.sql**.
> Don't forget to point the BLOBs in the *img* folder to the correct rows in the APP_IMAGES table. Names match.

## Credentials
|Username | Password| Cust_ID (for Referral)|
|---------|---------|---|
|test|`test`|C0001
|test2|`test2`|C0002
|test3|`test3`|C0003


## Problems
Hashing does not work correctly. At first it would only post memory addresses to the database, which cannot be used since they change every time, but when making a string out of it, it would print a concatenation of the password + salt.
