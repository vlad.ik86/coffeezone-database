-- Author Vladyslav Berezhnyak
-- Created by Vertabelo (http://vertabelo.com)

-- Drop all tables
DROP TABLE LOG;
DROP TABLE CART;
DROP TABLE CUSTOMERS;
DROP TABLE INGREDIENTS;
DROP TABLE ORDERS;
DROP TABLE PRODUCTS;
DROP TABLE RECIPE;
DROP TABLE RECIPE_INGREDIENTS;
DROP TABLE APP_IMAGES;

-- tables
-- Table: LOG
CREATE TABLE LOG (
    log_num varchar2(5)  NOT NULL,
    cust_id varchar2(5)  NOT NULL,
    username varchar2(30)  NOT NULL,
    time timestamp  NOT NULL,
    CONSTRAINT LOG_pk PRIMARY KEY (log_num)
) ;

-- Table: CART
CREATE TABLE CART (
    cart_id varchar2(5)  NOT NULL,
    ord_id varchar2(5)  NOT NULL,
    p0001qty number(3) DEFAULT 0,
    p0002qty number(3) DEFAULT 0,
    p0003qty number(3) DEFAULT 0,
    p0004qty number(3) DEFAULT 0,
    p0005qty number(3) DEFAULT 0,
    p0006qty number(3) DEFAULT 0,
    p0007qty number(3) DEFAULT 0,
    p0008qty number(3) DEFAULT 0,
    totalPrice number(4,2) DEFAULT 0,
    CONSTRAINT CART_pk PRIMARY KEY (cart_id)
) ;

-- Table: CUSTOMERS
CREATE TABLE CUSTOMERS (
    cust_id varchar2(5)  NOT NULL,
    username varchar2(32)  NULL,
    pass varchar2(128)  NULL,
    address varchar2(50)  NULL,
    phone varchar2(15)  NULL,
    email varchar2(30)  NULL,
    referral_id varchar2(5)  NULL,
    salt varchar2(32) NULL,
    discount_amount_available number(2)  DEFAULT 0,
    CONSTRAINT CUSTOMERS_pk PRIMARY KEY (cust_id)
) ;

-- Table: INGREDIENTS
CREATE TABLE INGREDIENTS (
    ingr_id varchar2(5)  NOT NULL,
    name varchar2(20)  NULL,
    restock_price number(5,2)  NULL,
    instock_qty number(6)  NULL,
    CONSTRAINT INGREDIENTS_pk PRIMARY KEY (ingr_id)
) ;

-- Table: ORDERS
CREATE TABLE ORDERS (
    ord_id varchar2(5)  NOT NULL,
    address varchar2(50)  NULL,
    date_placed date  NULL,
    cust_id varchar2(5)  NOT NULL,
    CONSTRAINT ORDERS_pk PRIMARY KEY (ord_id)
) ;

-- Table: PRODUCTS
CREATE TABLE PRODUCTS (
    prod_id varchar2(5)  NOT NULL,
    name varchar2(20)  NULL,
    retail number(3,2)  NULL,
    recipe_id varchar2(5)  NULL,
    product_img BLOB NULL,
    CONSTRAINT PRODUCTS_pk PRIMARY KEY (prod_id)
) ;

-- Table: APP_IMAGES
CREATE TABLE APP_IMAGES (
    image_name varchar2(20), 
    image_blob BLOB
) ;

-- Table: RECIPE
CREATE TABLE RECIPE (
    recipe_id varchar2(5)  NOT NULL,
    prod_id varchar2(5)  NOT NULL,
    CONSTRAINT RECIPE_pk PRIMARY KEY (recipe_id)
) ;

-- Table: RECIPE_INGREDIENTS
CREATE TABLE RECIPE_INGREDIENTS (
    ingr_id varchar2(5)  NOT NULL,
    qty number(5)  NULL,
    recipe_id varchar2(5)  NOT NULL,
    CONSTRAINT RECIPE_INGREDIENTS_pk PRIMARY KEY (recipe_id,ingr_id)
) ;

-- foreign keys
-- Reference: LOG_CUSTOMERS (table: LOG)
ALTER TABLE LOG ADD CONSTRAINT LOG_CUSTOMERS
    FOREIGN KEY (cust_id)
    REFERENCES CUSTOMERS (cust_id);
    
-- Reference: CART_ORDERS (table: CART)
ALTER TABLE CART ADD CONSTRAINT CART_ORDERS
    FOREIGN KEY (ord_id)
    REFERENCES ORDERS (ord_id);

-- Reference: ORDERS_CUSTOMERS (table: ORDERS)
ALTER TABLE ORDERS ADD CONSTRAINT ORDERS_CUSTOMERS
    FOREIGN KEY (cust_id)
    REFERENCES CUSTOMERS (cust_id);

-- Reference: RECIPE_INGREDIENTS_INGREDIENTS (table: RECIPE_INGREDIENTS)
ALTER TABLE RECIPE_INGREDIENTS ADD CONSTRAINT RECIPE_INGREDIENTS_INGREDIENTS
    FOREIGN KEY (ingr_id)
    REFERENCES INGREDIENTS (ingr_id);

-- Reference: RECIPE_INGREDIENTS_RECIPE (table: RECIPE_INGREDIENTS)
ALTER TABLE RECIPE_INGREDIENTS ADD CONSTRAINT RECIPE_INGREDIENTS_RECIPE
    FOREIGN KEY (recipe_id)
    REFERENCES RECIPE (recipe_id);

-- Reference: RECIPE_PRODUCTS (table: RECIPE)
ALTER TABLE RECIPE ADD CONSTRAINT RECIPE_PRODUCTS
    FOREIGN KEY (prod_id)
    REFERENCES PRODUCTS (prod_id);

-- End of file.

