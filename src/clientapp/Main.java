/**
 * @author Nicolae
 */

package clientapp;
	
import javafx.application.Application;
import java.sql.*;

import clientapp.backend.JLServices;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;

import java.io.InputStream;
import java.math.BigInteger;

public class Main extends Application {
	public Stage primaryStage;
	public static String username = null;
	public static Image[] images = new Image[5];
	
	@Override
	public void start(Stage primaryStage) {
		try {
			JLServices.getConnection(); //Database Connection
			//FXML
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(Main.class.getResource("Welcome.fxml"));
	        
	        //Get Images
	        PreparedStatement stmt = JLServices.conn.prepareStatement("SELECT image_blob FROM APP_IMAGES");
	        ResultSet rs = stmt.executeQuery();
	        
	        for(int i = 0; i < 5; i++) {
	        	rs.next();
	        	InputStream bs = rs.getBinaryStream("image_blob");
	        	images[i] = new Image(bs);
	        	System.out.println("got an image from database");
	        }
	        stmt.close();
	        
	        VBox vbox = loader.<VBox>load();
	        Scene scene = new Scene(vbox);
			
			//Set Scene
			primaryStage.setScene(scene);
			primaryStage.setTitle("Anteiku Shop [Guest]");
			primaryStage.setHeight(800);
			primaryStage.setWidth(1280);
			primaryStage.initStyle(StageStyle.DECORATED);
			primaryStage.setResizable(false);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
