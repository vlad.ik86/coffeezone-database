/**
 * @author Nicolae
 */
package clientapp;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class WelcomeController extends Main implements Initializable {
	@FXML
	Button browseBtn = new Button();
	@FXML
	ImageView image_logo_orange = new ImageView();
	@FXML
	ImageView image_logo_black = new ImageView();
	@FXML
	ImageView image_main_coffee = new ImageView();
	@FXML
	ImageView image_user = new ImageView();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		image_logo_orange.setImage(Main.images[0]);
		image_logo_black.setImage(Main.images[4]);
		image_user.setImage(Main.images[3]);
		image_main_coffee.setImage(Main.images[1]);
	}
	
	@FXML
    public void goToMenuGuest() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("MenuGuest.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToLogin() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToRegister() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Register.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToWelcome() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Welcome.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
}
