/**
 * @author Nicolae
 */
package clientapp;

import java.net.URL;
import java.sql.*;
import java.util.Arrays;
import java.util.ResourceBundle;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import clientapp.backend.JLServices;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController extends Main implements Initializable {
	@FXML
	Button browseBtn = new Button();
	@FXML
	private TextField username = new TextField();
	@FXML
	private PasswordField password = new PasswordField();
	@FXML
	private Text deliveryAddress = new Text();
	
	private String salt;
	private String hashDb;
	
	@FXML
	ImageView image_logo_orange = new ImageView();
	@FXML
	ImageView image_logo_black = new ImageView();
	@FXML
	ImageView image_main_coffee = new ImageView();
	@FXML
	ImageView image_user = new ImageView();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		image_logo_orange.setImage(Main.images[0]);
		image_logo_black.setImage(Main.images[4]);
		image_user.setImage(Main.images[3]);
		image_main_coffee.setImage(Main.images[1]);
	}
	
	public void goToWelcome() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Welcome.fxml"));
			Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToRegister() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Register.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToMenuGuest() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("MenuGuest.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	// After login
	public void goToMenu() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Menu.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setTitle("Anteiku Shop ["+Main.username+"]");
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void loginUser() throws SQLException {
		try {
			CallableStatement stmtLogin = JLServices.conn.prepareCall("{call login(?,?,?)}");
			stmtLogin.setString(1, username.getText());
			stmtLogin.registerOutParameter(2, java.sql.Types.VARCHAR);
			stmtLogin.registerOutParameter(3, java.sql.Types.VARCHAR);
			stmtLogin.execute();
			salt = stmtLogin.getString(2);
			hashDb = stmtLogin.getString(3);
			stmtLogin.close();
			
			byte[] hash = getHashedPassword();
			String hashClient = new String(hash);
			System.out.println("Database Salt: " +salt);
			System.out.println("Database Hash: " + hashDb);
			System.out.println("Client Hash: " + hashClient);
			if(hashDb != null) {
				if(hashDb.equals(hashClient)) {
					Main.username = username.getText();
					JLServices.conn.commit();
					goToMenu();
				} else {
					JLServices.conn.rollback();
					Alert loginError = new Alert(Alert.AlertType.ERROR);
					loginError.setTitle("Login Error");
					loginError.setHeaderText("Oops! Password is incorrect!");
					loginError.showAndWait();
				}
			} else {
				JLServices.conn.rollback();
				Alert loginError = new Alert(Alert.AlertType.ERROR);
				loginError.setTitle("Login Error");
				loginError.setHeaderText("Oops! Username is incorrect! Case-sensitivity is on.");
				loginError.showAndWait();
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			JLServices.conn.rollback();
		}
	}
	
	// Gets client password hashed using the database salt matching the username
	public byte[] getHashedPassword() {
		try {
			String hash_algorithm = "PBEWithSHA1AndDESede";
			
			PBEKeySpec spec = new PBEKeySpec((password.getText() + salt).toCharArray());
			SecretKeyFactory skf = SecretKeyFactory.getInstance(hash_algorithm);
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return hash;
			
		} catch (Exception e) {
			return null;
		}
	}
}
