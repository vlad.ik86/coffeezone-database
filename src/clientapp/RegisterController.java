/**
 * @author Nicolae
 */
package clientapp;
import clientapp.backend.*;

import java.math.BigInteger;
import java.net.URL;
import java.security.SecureRandom;
import java.sql.*;
import java.util.ResourceBundle;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class RegisterController extends Main implements Initializable{
	@FXML
	Button browseBtn = new Button();
	@FXML
	private TextField username = new TextField();
	@FXML
	private PasswordField password = new PasswordField();
	@FXML
	private TextField address = new TextField();
	@FXML
	private TextField email = new TextField();
	@FXML
	private TextField phonenumber = new TextField();
	@FXML
	private TextField referral = new TextField();
	SecureRandom rand = new SecureRandom();
	String salt = new BigInteger(130, rand).toString(32);
	
	@FXML
	ImageView image_logo_orange = new ImageView();
	@FXML
	ImageView image_logo_black = new ImageView();
	@FXML
	ImageView image_main_coffee = new ImageView();
	@FXML
	ImageView image_user = new ImageView();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		image_logo_orange.setImage(Main.images[0]);
		image_logo_black.setImage(Main.images[4]);
		image_user.setImage(Main.images[3]);
		image_main_coffee.setImage(Main.images[1]);
	}
	
	@FXML
	public void registerUser() throws SQLException {
		if (username.getText().isEmpty() || password.getText().isEmpty() || address.getText().isEmpty() || email.getText().isEmpty() || phonenumber.getText().isEmpty()) {
			Alert registerError = new Alert(Alert.AlertType.ERROR);
			registerError.setTitle("Register Error");
			registerError.setHeaderText("Oops! Please fill out all fields (Referral is optional)");
			registerError.showAndWait();
		} else {
			try {
				byte[] hash = getHashedPassword();
				String hashString = new String(hash);
				CallableStatement stmtRegister = JLServices.conn.prepareCall("{call register(?, ?, ?, ?, ?, ?, ?)}");
				stmtRegister.setString(1, username.getText());
				stmtRegister.setString(2, hashString);
				stmtRegister.setString(3, address.getText());
				stmtRegister.setString(4, phonenumber.getText());
				stmtRegister.setString(5, email.getText());
				stmtRegister.setString(6, referral.getText());
				stmtRegister.setString(7, salt);
				System.out.println("Username: " +username.getText());
				System.out.println("Pw: " +password.getText());
				System.out.println("Hash: " +hashString);
				System.out.println("Salt: " +salt);
				stmtRegister.execute();
				JLServices.conn.commit();
				stmtRegister.close();
				System.out.println("--- User Registered! Commited to Database ---");
				goToLogin();
			} catch (SQLException e) {
				e.printStackTrace();
				JLServices.conn.rollback();
			}
		}	
	}
	
	@FXML
    public void goToMenuGuest() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("MenuGuest.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToLogin() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	public byte[] getHashedPassword() {
		try {
			String hash_algorithm = "PBEWithSHA1AndDESede";
			
			PBEKeySpec spec = new PBEKeySpec((password.getText() + salt).toCharArray());
			SecretKeyFactory skf = SecretKeyFactory.getInstance(hash_algorithm);
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return hash;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	
	
	@FXML
	public void goToWelcome() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Welcome.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
}
