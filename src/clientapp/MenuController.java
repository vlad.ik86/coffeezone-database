/**
 * @author Nicolae
 */
package clientapp;
import clientapp.backend.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import oracle.sql.NUMBER;

public class MenuController extends Main implements Initializable {
	@FXML
	Button browseBtn = new Button();
	@FXML
	private TextField deliveryAddress = new TextField();
	@FXML
	private Text totalPrice = new Text();
	@FXML
	private Text discountAmountText = new Text();
	
	double totalPriceDouble = 0.00;
	BigDecimal totalPriceBD = new BigDecimal("0.00");
	BigDecimal lattePrice = new BigDecimal("9.85");
	BigDecimal cappuccinoPrice = new BigDecimal("7.75");
	BigDecimal espressoPrice = new BigDecimal("4.75");
	BigDecimal decafPrice = new BigDecimal("5.50");
	BigDecimal mocaPrice = new BigDecimal("6.95");
	BigDecimal macchiatoPrice = new BigDecimal("4.95");
	BigDecimal americanoPrice = new BigDecimal("9.95");
	BigDecimal doppioPrice = new BigDecimal("4.95");
	int discountAmount = 0;
	String address;
	
	@FXML
	ImageView image_coffee1 = new ImageView();
	@FXML
	ImageView image_coffee2 = new ImageView();
	@FXML
	ImageView image_coffee3 = new ImageView();
	@FXML
	ImageView image_coffee4 = new ImageView();
	@FXML
	ImageView image_coffee5 = new ImageView();
	@FXML
	ImageView image_coffee6 = new ImageView();
	@FXML
	ImageView image_coffee7 = new ImageView();
	@FXML
	ImageView image_coffee8 = new ImageView();
	@FXML
	ImageView image_logo_black = new ImageView();
	@FXML
	ImageView image_user = new ImageView();
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		image_coffee1.setImage(Main.images[2]);
		image_coffee2.setImage(Main.images[2]);
		image_coffee3.setImage(Main.images[2]);
		image_coffee4.setImage(Main.images[2]);
		image_coffee5.setImage(Main.images[2]);
		image_coffee6.setImage(Main.images[2]);
		image_coffee7.setImage(Main.images[2]);
		image_coffee8.setImage(Main.images[2]);
		image_logo_black.setImage(Main.images[4]);
		image_user.setImage(Main.images[3]);
		
		if (Main.username != null && !Main.username.trim().isEmpty()) {
			try {
				checkForDiscount();
				discountAmountText.setText("- $" + Integer.toString(discountAmount) + " (DISCOUNT)");
				checkForAddress();
				deliveryAddress.setText(address);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Latte
	@FXML
	Button lattePlus = new Button();
	@FXML
	Button latteMinus = new Button();
	@FXML
	Text latteQty = new Text();
	int latteQtyInt = 0;
	
	public void addLatte() {
		latteQtyInt++;
		latteQty.setText(Integer.toString(latteQtyInt));
		totalPriceBD = totalPriceBD.add(lattePrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeLatte() {
		if (latteQtyInt > 0) {
			latteQtyInt--;
			latteQty.setText(Integer.toString(latteQtyInt));
			totalPriceBD = totalPriceBD.subtract(lattePrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}

	// Cappuccino
	@FXML
	Button cappuccinoPlus = new Button();
	@FXML
	Button cappuccinoMinus = new Button();
	@FXML
	Text cappuccinoQty = new Text();
	int cappuccinoQtyInt = 0;
	
	public void addCappuccino() {
		cappuccinoQtyInt++;
		cappuccinoQty.setText(Integer.toString(cappuccinoQtyInt));
		totalPriceBD = totalPriceBD.add(cappuccinoPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeCappuccino() {
		if (cappuccinoQtyInt > 0) {
			cappuccinoQtyInt--;
			cappuccinoQty.setText(Integer.toString(cappuccinoQtyInt));
			totalPriceBD = totalPriceBD.subtract(cappuccinoPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	// Espresso
	@FXML
	Button espressoPlus = new Button();
	@FXML
	Button espressoMinus = new Button();
	@FXML
	Text espressoQty = new Text();
	int espressoQtyInt = 0;
	
	public void addEspresso() {
		espressoQtyInt++;
		espressoQty.setText(Integer.toString(espressoQtyInt));
		totalPriceBD = totalPriceBD.add(espressoPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeEspresso() {
		if (espressoQtyInt > 0) {
			espressoQtyInt--;
			espressoQty.setText(Integer.toString(espressoQtyInt));
			totalPriceBD = totalPriceBD.subtract(espressoPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	// Decaf
	@FXML
	Button decafPlus = new Button();
	@FXML
	Button decafMinus = new Button();
	@FXML
	Text decafQty = new Text();
	int decafQtyInt = 0;
	
	public void addDecaf() {
		decafQtyInt++;
		decafQty.setText(Integer.toString(decafQtyInt));
		totalPriceBD = totalPriceBD.add(decafPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeDecaf() {
		if (decafQtyInt > 0) {
			decafQtyInt--;
			decafQty.setText(Integer.toString(decafQtyInt));
			totalPriceBD = totalPriceBD.subtract(decafPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	// Moca
	@FXML
	Button mocaPlus = new Button();
	@FXML
	Button mocaMinus = new Button();
	@FXML
	Text mocaQty = new Text();
	int mocaQtyInt = 0;
	
	public void addMoca() {
		mocaQtyInt++;
		mocaQty.setText(Integer.toString(mocaQtyInt));
		totalPriceBD = totalPriceBD.add(mocaPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeMoca() {
		if (mocaQtyInt > 0) {
			mocaQtyInt--;
			mocaQty.setText(Integer.toString(mocaQtyInt));
			totalPriceBD = totalPriceBD.subtract(mocaPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	// Macchiato
	@FXML
	Button macchiatoPlus = new Button();
	@FXML
	Button macchiatoMinus = new Button();
	@FXML
	Text macchiatoQty = new Text();
	int macchiatoQtyInt = 0;
	
	public void addMacchiato() {
		macchiatoQtyInt++;
		macchiatoQty.setText(Integer.toString(macchiatoQtyInt));
		totalPriceBD = totalPriceBD.add(macchiatoPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeMacchiato() {
		if (macchiatoQtyInt > 0) {
			macchiatoQtyInt--;
			macchiatoQty.setText(Integer.toString(macchiatoQtyInt));
			totalPriceBD = totalPriceBD.subtract(macchiatoPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	// Americano
	@FXML
	Button americanoPlus = new Button();
	@FXML
	Button americanoMinus = new Button();
	@FXML
	Text americanoQty = new Text();
	int americanoQtyInt = 0;
	
	public void addAmericano() {
		americanoQtyInt++;
		americanoQty.setText(Integer.toString(americanoQtyInt));
		totalPriceBD = totalPriceBD.add(americanoPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeAmericano() {
		if (americanoQtyInt > 0) {
			americanoQtyInt--;
			americanoQty.setText(Integer.toString(americanoQtyInt));
			totalPriceBD = totalPriceBD.subtract(americanoPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	// Doppio
	@FXML
	Button doppioPlus = new Button();
	@FXML
	Button doppioMinus = new Button();
	@FXML
	Text doppioQty = new Text();
	int doppioQtyInt = 0;
	
	public void addDoppio() {
		doppioQtyInt++;
		doppioQty.setText(Integer.toString(doppioQtyInt));
		totalPriceBD = totalPriceBD.add(doppioPrice);
		totalPrice.setText("$" + totalPriceBD.toString());
	}
	
	public void removeDoppio() {
		if (doppioQtyInt > 0) {
			doppioQtyInt--;
			doppioQty.setText(Integer.toString(doppioQtyInt));
			totalPriceBD = totalPriceBD.subtract(doppioPrice);
			totalPrice.setText("$" + totalPriceBD.toString());
		} else {
			System.out.println("Quantity at minimum");
		}
	}
	
	public void checkForDiscount() throws SQLException {
		try {
			CallableStatement stmtCheckDiscount = JLServices.conn.prepareCall("{? = call getDiscountAmountAvailable(?)}");
			stmtCheckDiscount.registerOutParameter((1), Types.INTEGER);
			stmtCheckDiscount.setString((2), Main.username);
			stmtCheckDiscount.execute();
			discountAmount = stmtCheckDiscount.getInt(1);
			System.out.println("Discount amount: " + discountAmount);
			JLServices.conn.commit();
			stmtCheckDiscount.close();
		} catch (SQLException e) {
			JLServices.conn.rollback();
			e.printStackTrace();
		}
	}
	
	public void checkForAddress() throws SQLException {
		try {
			CallableStatement stmtGetAddress = JLServices.conn.prepareCall("{? = call getAddress(?)}");
			stmtGetAddress.registerOutParameter((1), Types.VARCHAR);
			stmtGetAddress.setString((2), Main.username);
			stmtGetAddress.execute();
			address = stmtGetAddress.getString(1);
			System.out.println("Address from DB: " + address);
			JLServices.conn.commit();
			stmtGetAddress.close();
		} catch (SQLException e) {
			JLServices.conn.rollback();
			e.printStackTrace();
		}
	}
	
	@FXML
	public void goToWelcome() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Welcome.fxml"));
	        Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        oldStage.setTitle("Anteiku Shop [Guest]");
	        Main.username = "";
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void goToLogin() {
		try {
	        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
			Stage oldStage = (Stage) browseBtn.getScene().getWindow();
	        oldStage.setScene(new Scene(root));
	        oldStage.setTitle("Anteiku Shop [Guest]");
	        Main.username = "";
	    }catch (Exception e){
	        e.printStackTrace();
	    }
    }
	
	@FXML
	public void setAddress() throws SQLException {
			try {
				System.out.println("--- Preparing setAddress ---");
				CallableStatement stmtSetAddress = JLServices.conn.prepareCall("{call setAddress(?, ?)}");
				stmtSetAddress.setString(1, Main.username);
				stmtSetAddress.setString(2, deliveryAddress.getText());
				System.out.println("Username: "+ Main.username);
				System.out.println("Delivery Address: " + deliveryAddress.getText());
				stmtSetAddress.execute();
				JLServices.conn.commit();
				stmtSetAddress.close();
				System.out.println("--- Address Set! Commited to Database ---");
			} catch (SQLException e) {
				e.printStackTrace();
				JLServices.conn.rollback();
			}
		}	

	@FXML
	public void checkoutCart() throws SQLException {
		try {
			setAddress();
			System.out.println("Total Price pre-discount: " + totalPriceBD);
			if (totalPriceBD.compareTo(BigDecimal.valueOf(discountAmount)) > 0) {
				totalPriceBD = totalPriceBD.subtract(BigDecimal.valueOf(discountAmount));
				System.out.println("Total Price post-discount: " + totalPriceBD);
				totalPriceBD = totalPriceBD.setScale(2, RoundingMode.FLOOR);
				
				try {
					CallableStatement stmtCheckout = JLServices.conn.prepareCall("{call checkout(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
					stmtCheckout.setString(1, Main.username);
					stmtCheckout.setInt(2, latteQtyInt);
					stmtCheckout.setInt(3, cappuccinoQtyInt);
					stmtCheckout.setInt(4, espressoQtyInt);
					stmtCheckout.setInt(5, decafQtyInt);
					stmtCheckout.setInt(6, mocaQtyInt);
					stmtCheckout.setInt(7, macchiatoQtyInt);
					stmtCheckout.setInt(8, americanoQtyInt);
					stmtCheckout.setInt(9, doppioQtyInt);
					stmtCheckout.setBigDecimal(10, totalPriceBD);
					System.out.println("--- All ? set, before execute ---");
					stmtCheckout.execute();
					System.out.println("--- Post execute ---");
					JLServices.conn.commit();
					stmtCheckout.close();
				} catch (SQLException e){
					e.printStackTrace();
					JLServices.conn.rollback();
				}
				Alert checkoutSuccess = new Alert(Alert.AlertType.INFORMATION);
				checkoutSuccess.setTitle("Order Placed");
				checkoutSuccess.setHeaderText("Your order has been placed! You will be redirected to the Welcome Page.");
				checkoutSuccess.show();
				goToWelcome();
			} else {
				Alert checkoutError = new Alert(Alert.AlertType.ERROR);
				checkoutError.setTitle("Checkout Error");
				checkoutError.setHeaderText("Oops! The total must be greater than your available discount!");
				checkoutError.show();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			JLServices.conn.rollback();
		}
	}	
}
