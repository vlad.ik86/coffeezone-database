
-- Author Luke Weaver
-- This procedure will set a specified customer's address
CREATE OR REPLACE PROCEDURE setAddress (vUsername IN VARCHAR2, vAddress IN VARCHAR2)
AS
    custCount NUMBER(3);
BEGIN
    SELECT COUNT(c.username) INTO custCount FROM customers c
    WHERE c.username = vUsername;
    
    IF (custCount = 1) THEN
        UPDATE customers
        SET address = vAddress
        WHERE username = vUsername;
    END IF;
END;
/
-- Author Luke Weaver
-- This procedure gets the address of a specified user
CREATE OR REPLACE FUNCTION getAddress (vUsername IN VARCHAR2) RETURN VARCHAR2
AS
    addr VARCHAR2(30);
BEGIN
    SELECT c.address INTO addr FROM customers c
    WHERE username = vUsername;
    RETURN addr;
END;
/
-- Author Luke Weaver
-- This function gets the discount available for a specified customer
CREATE OR REPLACE FUNCTION getDiscountAmountAvailable (vUsername IN VARCHAR2) RETURN NUMBER
AS
    discount VARCHAR(2);
BEGIN
    SELECT c.discount_amount_available INTO discount FROM customers c
    WHERE c.username = vUsername;
    
    RETURN discount;
END;
/
-- Author Nicolae Rusu
-- This procedure calls for newOrder and updates the Cart table with the quantity of each coffee and total price.
CREATE OR REPLACE PROCEDURE checkout (vUsername IN VARCHAR2, vLatteQty IN NUMBER, vCappuccinoQty IN NUMBER,
    vEspressoQty IN NUMBER, vDecafQty IN NUMBER, vMocaQty IN NUMBER, vMacchiatoQty IN NUMBER,
    vAmericanoQty IN NUMBER, vDoppioQty IN NUMBER, vTotalPrice IN NUMBER)
AS
    ingredientsAvailable CHAR (1);
    vCustID CUSTOMERS.CUST_ID%TYPE;
    vCartID CART.CART_ID%TYPE;
BEGIN
    ingredientsAvailable := compileTotalIngredients(vLatteQty, vCappuccinoQty, vEspressoQty, vDecafQty, vMocaQty, vMacchiatoQty, vAmericanoQty, vDoppioQty);
    IF(ingredientsAvailable = 'y') THEN
        SELECT cust_id INTO vCustID FROM customers
        WHERE username = vUsername;
        vCartID := newOrder(vCustID);
        UPDATE cart
            SET p0001qty = vLatteQty,
            p0002qty = vCappuccinoQty,
            p0003Qty = vEspressoQty,
            p0004Qty = vDecafQty,
            p0005Qty = vMocaQty,
            p0006Qty = vMacchiatoQty,
            p0007Qty = vAmericanoQty,
            p0008Qty = vDoppioQty, 
            totalPrice = vTotalPrice
            WHERE cart_id = vCartID;
        UPDATE customers
            SET discount_amount_available = 0
            WHERE cust_id = vCustID;
    END IF;
END;
/

-- Author Vladyslav Berezhnyak, Nicolae Rusu
-- This function takes as input a cust_id and fills all the columns in the ORDER table, effectively
-- creating a new ORDER. It also creates and return a cart_id, in order to show which cart to fill for that order.
-- Also modifies CART table.
CREATE OR REPLACE FUNCTION newOrder(vCustId IN VARCHAR2) RETURN VARCHAR2
AS
    vAddress VARCHAR(50);
    vDate_placed DATE := sysdate;
    vDate_sent DATE := vDate_placed + 5;
    vOrd_id VARCHAR2(5);
    vOrdCount NUMBER(3);
    vNewCart VARCHAR2(5);
BEGIN
    SELECT COUNT(ord_id) + 1 INTO vOrdCount FROM ORDERS;
    SELECT address INTO vAddress FROM CUSTOMERS
    WHERE cust_id = vCustId;
    
    IF(vOrdCount < 10) THEN
        vOrd_id := CONCAT('OR00', vOrdCount);
        vNewCart := CONCAT('CT00', vOrdCount);
    ELSIF(vOrdCount < 100) THEN
        vOrd_id := CONCAT('OR0', vOrdCount);
        vNewCart := CONCAT('CT0', vOrdCount);
    END IF;
    
    INSERT INTO orders
    VALUES(vOrd_id, vAddress, vDate_placed, vCustId);
    
    INSERT INTO cart
    VALUES(vNewCart, vOrd_id, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    
    return vNewCart;
END;
/
-- Author Vladyslav Berezhnyak
-- This checks if the quantity of ingredients is valid to be made into coffees.
-- If it is, then returns 'y', if not, 'n'.
CREATE OR REPLACE FUNCTION checkCanAddToCart(in001 IN NUMBER, in002 IN NUMBER, in003 IN NUMBER, in004 IN NUMBER, in005 IN NUMBER, 
in006 IN NUMBER, in007 IN NUMBER, in008 IN NUMBER) RETURN CHAR
AS
    vIngId VARCHAR2(5);
    stock NUMBER(5) := 0;
    bool CHAR(1) := 'y';
BEGIN
    dbms_output.put_line('starting checking for stock!');
    
    IF(in001 > 0) THEN
        vIngId := 'IN001';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in001 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in001!');
    END IF;
    
    IF(in002 > 0) THEN
        vIngId := 'IN002';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in002 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in002!');
    END IF;
    
    IF(in003 > 0) THEN
        vIngId := 'IN003';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in003 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in003!');
    END IF;
    
    IF(in004 > 0) THEN
        vIngId := 'IN004';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in004 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in004!');
    END IF;
    
    IF(in005 > 0) THEN
        vIngId := 'IN005';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in005 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in005!');
    END IF;
    
    IF(in006 > 0) THEN
        vIngId := 'IN006';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in006 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in006!');
    END IF;
    
    IF(in007 > 0) THEN
        vIngId := 'IN007';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in004 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in007!');
    END IF;
    
    IF(in008 > 0) THEN
        vIngId := 'IN008';
        SELECT instock_qty INTO stock FROM ingredients
        WHERE ingr_id = vIngId;
        IF(in008 > stock) THEN
            bool := 'n';
            RETURN bool;
        END IF;
        dbms_output.put_line('done checking in008!');
    END IF;
    
    dbms_output.put_line('ended checking for stock!');
    RETURN bool;
END;
/
-- Author Vladyslav Berezhnyak
-- This function takes as input all of the coffees and compiles all of their ingredients together.
-- Then calls canAddToCart and return boolean 
CREATE OR REPLACE FUNCTION compileTotalIngredients(vLatte IN NUMBER, vCappuccino IN NUMBER , vEspresso IN NUMBER, vDecaf IN NUMBER, vMoca IN NUMBER, 
vMacchiato IN NUMBER, vAmericano IN NUMBER, vDoppio IN NUMBER) RETURN CHAR
AS
    vRecipe VARCHAR2(5);
    
    CURSOR curs IS(
        SELECT ingr_id, qty FROM recipe_ingredients
         WHERE recipe_id = vRecipe
    );
    
    TYPE array IS TABLE OF NUMBER
    INDEX BY VARCHAR2(5);
    ingredientsArr array;
    ing_index VARCHAR2(5);
    qty NUMBER(5) := 0;
    temp NUMBER(5) := 0;
    bool CHAR (1);
BEGIN
    dbms_output.put_line('starting ingredient compilation!');

    ingredientsArr('IN001') := 0;
    ingredientsArr('IN002') := 0;
    ingredientsArr('IN003') := 0;
    ingredientsArr('IN004') := 0;
    ingredientsArr('IN005') := 0;
    ingredientsArr('IN006') := 0;
    ingredientsArr('IN007') := 0;
    ingredientsArr('IN008') := 0;

    IF(vLatte > 0) THEN
        vRecipe := 'R0001';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vLatte);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking latte!');
    END IF;
    
    IF(vCappuccino > 0) THEN
        vRecipe := 'R0002';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vCappuccino);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking capp!');
    END IF;
    
    IF(vEspresso > 0) THEN
        vRecipe := 'R0003';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vEspresso);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking esp!');
    END IF;
    
    IF(vDecaf > 0) THEN
        vRecipe := 'R0004';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vDecaf);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking decaf');
    END IF;
    
    IF(vMoca > 0) THEN
        vRecipe := 'R0005';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vMoca);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking moca!');
    END IF;
    
    IF(vMacchiato > 0) THEN
        vRecipe := 'R0006';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vMacchiato);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking macc!');
    END IF;
    
    IF(vAmericano > 0) THEN
        vRecipe := 'R0007';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vAmericano);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking ameri!');
    END IF;
    
    IF(vDoppio > 0) THEN
        vRecipe := 'R0008';
        OPEN curs;
        LOOP
            FETCH curs INTO ing_index, qty;
            temp := ingredientsArr(ing_index);
            ingredientsArr(ing_index) := temp + (qty * vDoppio);
            EXIT WHEN curs%NOTFOUND;
        END LOOP;
        CLOSE curs;
        dbms_output.put_line('done checking dopp!');
    END IF;
    
    dbms_output.put_line('ended ingredient compilation!');
    
    bool := checkCanAddToCart(ingredientsArr('IN001'), ingredientsArr('IN002'), ingredientsArr('IN003'), ingredientsArr('IN004'), 
    ingredientsArr('IN005'), ingredientsArr('IN006'), ingredientsArr('IN007'), ingredientsArr('IN008'));
    
    RETURN bool;
END;
/
/* testing for compileTotalIngredients and checkCanAddToCart
DECLARE
    latte NUMBER(3) := 1;
    capp NUMBER(3) := 1;
    espresso NUMBER(3) := 2;
    decaf NUMBER(3) := 1;
    moca NUMBER(3) := 1;
    macc NUMBER(3) := 1;
    ameri NUMBER(3) := 1;
    dopp NUMBER(3) := 5;
    ans CHAR(1);
BEGIN
    ans := compileTotalIngredients(latte, capp, espresso, decaf, moca, macc, ameri, dopp);
    dbms_output.put_line('finally:' || ans);
END;
*/

/*
    its supposed to trigger when its trying to make an order for something and theres not enough ingredients
    so when ingredients fall under 0
    and it should like add 100 or something 
*/
-- Author Vladyslav Berezhnyak
CREATE OR REPLACE TRIGGER restock_ings BEFORE UPDATE ON ingredients FOR EACH ROW
BEGIN
    IF :NEW.instock_qty < 100  THEN
        :NEW.instock_qty := :OLD.instock_qty + 500;
    END IF;
END;
/
-- Nicolae Rusu
-- This procedure takes in a username, and checks for a registered user with the same username.
-- If it exists, it logs the login attempt into a table, and returns the Salt and Hashed Password to the program.
CREATE OR REPLACE PROCEDURE login (vUsername IN VARCHAR2, vSalt OUT VARCHAR2, vHashedPw OUT VARCHAR2)
AS
    custCount NUMBER(3);
    custID VARCHAR2(5);
    time TIMESTAMP(6) := current_timestamp;
    logAmount NUMBER(4);
BEGIN
    SELECT COUNT(username) INTO custCount FROM customers
    WHERE username = vUsername;
    IF(custCount = 1) THEN
        SELECT cust_id INTO custID FROM customers
        WHERE username = vUsername;
        
        SELECT COUNT(log_num) INTO logAmount FROM log;
        logAmount := logAmount +1;
        
        INSERT INTO log
        VALUES (logAmount, custID, vUsername, time);
        SELECT pass, salt INTO vHashedPw, vSalt FROM customers
        WHERE username = vUsername;
    END IF;
END;
/
-- Author Luke Weaver, Nicolae Rusu
-- This procedure registers a new user, and adds referral bonus to the referring user.

CREATE OR REPLACE PROCEDURE register (vUsername IN VARCHAR2, vPw IN VARCHAR2, vAddress IN VARCHAR2, vPhone IN VARCHAR2, vEmail IN VARCHAR2, vRef_id IN VARCHAR2, vSalt IN VARCHAR2)
AS
    custIdCount NUMBER(3);
    custAmount NUMBER(3);
    customer_registered EXCEPTION;
    custId VARCHAR2(5);
    refIdUseCount NUMBER(2);
BEGIN
    SELECT COUNT(cust_id)+ 1 INTO custIdCount FROM customers;
    SELECT COUNT(c.username) INTO custAmount FROM customers c
    WHERE c.username = vUsername OR c.email = vEmail;
    
    IF (custAmount > 0) THEN
        RAISE customer_registered;
    END IF;
    
    dbms_output.put_line(custIdCount);
    
    IF(custIdCount < 10) THEN
        custId := CONCAT('C000', custIdCount);
    ELSIF(custIdCount < 100) THEN
        custId := CONCAT('C00', custIdCount);
    END IF;
    INSERT INTO customers VALUES(custId, vUsername, vPw, vAddress, vPhone, vEmail, vRef_id, vSalt, '0');
    
    SELECT COUNT(*) INTO refIdUseCount FROM customers
    WHERE referral_id = vRef_id;
    IF (refIdUseCount < 3) THEN
        UPDATE customers
        SET discount_amount_available = discount_amount_available + 5
        WHERE cust_id = vRef_id;
    END IF;
    
EXCEPTION
    WHEN customer_registered THEN
        dbms_output.put_line('Customer already registered');
END;
